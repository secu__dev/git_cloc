FROM alpine:latest

RUN apk add curl git perl \
 && curl https://raw.githubusercontent.com/AlDanial/cloc/master/cloc -o /usr/bin/cloc \
 && chmod a+x /usr/bin/cloc \
 && mkdir /app
WORKDIR /app